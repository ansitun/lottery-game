from django.contrib import admin


from account.models import UserProfile


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'supervisor', 'diamond', 'user_role', 'phone', 'last_active')


admin.site.register(UserProfile, UserProfileAdmin)

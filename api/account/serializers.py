from django.contrib.auth.models import User, Group
from rest_framework import serializers

from account.models import UserProfile


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class UserDetailSerializer(serializers.ModelSerializer):
    user_id = serializers.CharField(source='user.pk', read_only=True)
    user_name = serializers.CharField(source='user.username', read_only=True)
    supervisor_name = serializers.CharField(source='supervisor.username', read_only=True)
    supervisor_id = serializers.CharField(source='supervisor.pk', read_only=True)
    user_role = serializers.SerializerMethodField()

    def get_user_role(self, obj):
        return obj.get_user_role_display()

    class Meta:
        model = UserProfile
        fields = ['user_id', 'user_name', 'supervisor_name', 'supervisor_id', 'user_role', 'diamond', 'phone', 'last_active',]

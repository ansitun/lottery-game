from django.contrib.auth.models import User
from django.db import models


class UserProfile(models.Model):
    PLAYER = 'PLR'
    PENCILER = 'PEN'
    RIDER = 'RID'
    ADMIN = 'ADM'

    USER_ROLE_CHOICES = [
        (PLAYER, 'Player'),
        (PENCILER, 'Penciler'),
        (RIDER, 'Rider'),
        (ADMIN, 'Admin')
    ]

    user = models.OneToOneField(
        to=User, on_delete=models.SET_NULL, null=True,
        related_name='users',
        related_query_name="user"
    )
    phone = models.CharField(null=True, blank=True, max_length=255)
    diamond = models.IntegerField(null=True, blank=True)
    last_active = models.DateTimeField(null=True, blank=True)
    user_role = models.CharField(
        max_length=10,
        choices=USER_ROLE_CHOICES,
        default=PLAYER,
    )
    supervisor = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True,
        related_name='supervisors',
        related_query_name = "supervisor",
    )


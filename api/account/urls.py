from django.urls import path
from account import views

urlpatterns = [
    path('user_details/', views.UserDetails.as_view()),
]

from django.contrib import admin

from game.models import GameData, UserBet


class GameDataAdmin(admin.ModelAdmin):
    list_display = ('created', 'slot', 'digit3', 'digit1', 'rider_id')


admin.site.register(GameData, GameDataAdmin)


class UserBetAdmin(admin.ModelAdmin):
    list_display = ('user', 'slot', 'digit3', 'digit1', 'diamond', 'type', 'created', 'reward', 'did_win',
                    'is_claimed', 'get_rider')

    def get_rider(self, obj):
        return obj.user.users.supervisor.users.supervisor

    get_rider.short_description = 'Rider'
    # get_rider.admin_order_field = 'book__author'


admin.site.register(UserBet, UserBetAdmin)


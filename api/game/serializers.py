from rest_framework import serializers

from game.models import GameData, UserBet


class GameDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameData
        fields = ['id', 'digit3', 'digit1', 'slot', 'created', 'rider_id']


class UserBetSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()

    def get_type(self, obj):
        return obj.get_type_display()

    class Meta:
        model = UserBet
        fields = ['id', 'slot', 'digit3', 'digit1', 'created', 'diamond', 'type']


class UserBetWinSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()

    def get_type(self, obj):
        return obj.get_type_display()

    class Meta:
        model = UserBet
        fields = ['id', 'slot', 'digit3', 'digit1', 'created', 'diamond', 'type', 'reward', 'is_claimed']


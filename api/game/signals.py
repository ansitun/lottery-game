import traceback

from django.db import transaction
from django.db.models import F, Case, When, Q
from django.db.models.signals import post_save
from django.dispatch import receiver

from game.models import GameData, UserBet


@receiver(post_save, sender=GameData)
def create_user_profile(sender, instance, created, **kwargs):
    # Whenever a insert is done on GameData, declare winner for that date, slot.
    print('sender', sender)
    print('instance', instance.created)
    print('created', created)
    print('rider', instance.rider_id.pk)

    if not created:  # checking for insert.
        return

    winning_list = UserBet.objects.filter(
        Q(digit3=instance.digit3) | Q(digit1=instance.digit1),

        slot=instance.slot,

        created__year=instance.created.year,
        created__month=instance.created.month,
        created__day=instance.created.day,
    )

    print('winning list ', winning_list)

    for bet_obj in winning_list:

        bet_diamond = bet_obj.diamond  # diamond placed as bet by the user

        if bet_obj.type == UserBet.SINGLE:
            reward = bet_diamond * 9
        else:
            reward = bet_diamond * 10

        try:
            with transaction.atomic():
                bet_obj.reward = reward
                bet_obj.did_win = True
                bet_obj.is_claimed = True
                bet_obj.user.users.diamond = bet_obj.user.users.diamond + reward

                bet_obj.user.users.save()  # update UserProfile
                bet_obj.save()  # update UserBet
        except:
            print(traceback.format_exc())

    print('done')

# obj = UserBet.objects.filter(
#     slot=instance.slot,
#
#     created__year=instance.created.year,
#     created__month=instance.created.month,
#     created__day=instance.created.day,
#
#     digit3=instance.digit3,
#     digit1=instance.digit1,
#
# ).update(
#     did_win=True,
#     is_claimed=True,
#     reward=Case(
#         When(type=UserBet.SINGLE,
#              then=F('diamond') * 9),
#         When(type=UserBet.PATTI,
#              then=F('diamond') * 10),
#         default=0
#     )
# )
# print('obj', obj)

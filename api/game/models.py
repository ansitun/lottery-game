from django.contrib.auth.models import User
from django.db import models
from django.db.models import SET_NULL


class GameData(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    digit3 = models.IntegerField(null=True, blank=True)
    digit1 = models.IntegerField(null=True, blank=True)
    slot = models.IntegerField(null=True, blank=True)
    rider_id = models.ForeignKey(User, null=True, blank=True, on_delete=SET_NULL)

    class Meta:
        ordering = ['created']


class UserBet(models.Model):
    SINGLE = 'SGL'
    PATTI = 'PTI'

    SINGLE_BET_DISPLAY = 'single'
    PATTI_BET_DISPLAY = 'patti'

    BET_TYPE_CHOICES = [
        (SINGLE, SINGLE_BET_DISPLAY),
        (PATTI, PATTI_BET_DISPLAY)
    ]

    user = models.ForeignKey(
        to=User, on_delete=models.SET_NULL, null=True,
        related_name='user_bets',
        related_query_name="user_bet"
    )
    slot = models.IntegerField(null=True, blank=True)
    digit3 = models.IntegerField(null=True, blank=True)
    digit1 = models.IntegerField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    diamond = models.IntegerField(null=True, blank=True)
    type = models.CharField(
        max_length=10,
        choices=BET_TYPE_CHOICES,
        default=SINGLE,
    )
    did_win = models.BooleanField(default=False)
    reward = models.IntegerField(default=0)
    is_claimed = models.BooleanField(default=False)


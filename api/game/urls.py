from django.urls import path
from game import views

urlpatterns = [
    path('gamedata/', views.GameDataList.as_view()),
    path('lottery_history/', views.LotteryHistoryList.as_view()),
    path('bet/', views.UserBetApi.as_view()),
    path('played/', views.GetAllPlayedHistory.as_view()),
    path('win/', views.GetPlayedWin.as_view()),
    path('transfer/', views.TransferDiamond.as_view()),
]

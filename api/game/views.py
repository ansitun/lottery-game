import datetime
import traceback

from django.db.models import F
from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db import connection, transaction

from account.models import UserProfile
from game.models import GameData, UserBet
from game.serializers import GameDataSerializer, UserBetSerializer, UserBetWinSerializer


class GameDataList(APIView):
    """
    Get or publish a lottery data.
    """
    # permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get(self, request, format=None):
        game_data = GameData.objects.all()
        serializer = GameDataSerializer(game_data, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = GameDataSerializer(data=request.data)
        game_datum_id = GameData.objects.raw("select id from game_gamedata where slot = " +
                                          str(request.data['slot']) + " and DATE(created) = date('now')")
        count = len(game_datum_id)

        if game_datum_id is not None and len(game_datum_id) > 0:
            cursor = connection.cursor()
            cursor.execute("UPDATE game_gamedata SET digit3 = " + str(request.data['digit3'])
                           + ", digit1 = " + str(request.data['digit1']) + " WHERE id = " + str(game_datum_id[0].id))
            return Response({'success': True}, status=status.HTTP_201_CREATED)
        else:
            if serializer.is_valid():
                serializer.save()
                return Response({'success': True}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LotteryHistoryList(APIView):
    """
    Get lottery history published by the admin
    """
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get(self, request, format=None):

        try:
            from_date = datetime.datetime.strptime(request.GET.get('from'), '%Y-%m-%d')
        except:
            from_date = None

        try:
            # As to_date is yyyy-mm-dd 00:00:00, add one day to include the to_date in query.
            to_date = datetime.datetime.strptime(request.GET.get('to'), '%Y-%m-%d') + datetime.timedelta(days=1)
        except:
            to_date = None

        print('f-', from_date)
        print('to-', to_date)
        if from_date and to_date:
            game_data = GameData.objects.filter(
                created__gte=from_date,
                created__lte=to_date
            ).order_by('-created', '-slot')
        else:
            game_data = GameData.objects.all().order_by('-created', '-slot')

        result = []
        prev_date = None
        temp = dict()
        first_entry = True

        if len(game_data) == 1:
            item = game_data.first()
            temp['date'] = prev_date
            temp['slot' + str(item.slot)] = {"3digit": item.digit3, "1digit": item.digit1}

        for item in game_data:
            current_date = item.created.strftime('%Y-%m-%d')

            if current_date != prev_date:
                if not first_entry:
                    result.append(temp)
                    temp = dict()
                prev_date = current_date
            temp['date'] = prev_date
            temp['slot' + str(item.slot)] = {"3digit": item.digit3, "1digit": item.digit1}

            first_entry = False

        result.append(temp)

        return Response(result)


class UserBetApi(APIView):
    """
    Post a bet
    """
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def post(self, request, format=None):
        print('Initial diamond', request.user.users.diamond)
        print(request.data)
        user_balance_diamond = request.user.users.diamond

        slot = request.data.get('slot', None)
        if not slot or slot > 8:
            return Response(
                {'success': False, 'msg': 'Invalid slot no!'}, status=status.HTTP_400_BAD_REQUEST)

        bets = request.data.get('bet')
        if not bets:
            return Response(
                {'success': False, 'msg': 'Empty bet data!'}, status=status.HTTP_400_BAD_REQUEST)

        for bet in bets:
            input_data = dict()
            input_data['digit3'] = bet.get('3digit', 0)
            input_data['digit1'] = bet.get('1digit', 0)
            input_data['diamond'] = bet.get('diamond', 0)
            input_data['type'] = bet.get('type', '').lower()

            if input_data['type'] == UserBet.SINGLE_BET_DISPLAY:
                input_data['type'] = UserBet.SINGLE
            elif input_data['type'] == UserBet.PATTI_BET_DISPLAY:
                input_data['type'] = UserBet.PATTI

            # validate the available diamonds
            if input_data['diamond'] > user_balance_diamond:
                # update the balance diamond or the user.
                UserProfile.objects.filter(user=request.user.pk).update({'diamond': user_balance_diamond})
                return Response(
                    {'success': False, 'msg': 'Not enough diamonds!', 'diamond': user_balance_diamond},
                    status=status.HTTP_201_CREATED)

            user_balance_diamond = user_balance_diamond - input_data['diamond']

            input_data['slot'] = slot
            input_data['user'] = request.user

            # save user bet
            UserBet.objects.create(**input_data)

        # update the balance diamond or the user.
        UserProfile.objects.filter(user=request.user.pk).update(diamond=user_balance_diamond)

        result = {'success': True, 'diamond': user_balance_diamond}

        return Response(result, status=status.HTTP_201_CREATED)


class GetAllPlayedHistory(APIView):
    """
    Get played lottery history for a particular user till date.

    """
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get(self, request, format=None):

        try:
            # As to_date is yyyy-mm-dd 00:00:00, add one day to include the to_date in query.
            till_date = datetime.datetime.strptime(request.GET.get('till'), '%Y-%m-%d') + datetime.timedelta(days=1)
        except:
            till_date = None

        print('to-', till_date)
        if till_date:
            user_bet = UserBet.objects.filter(
                user=request.user,
                created__lte=till_date
            )
        else:
            user_bet = UserBet.objects.filter(
                user=request.user
            )

        serializer = UserBetSerializer(user_bet, many=True)
        return Response(serializer.data)


class GetPlayedWin(APIView):
    """
    Get played lottery history for a particular user till date.

    """
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get(self, request, format=None):

        try:
            input_date = request.GET.get('for').split('-')
            day = input_date[2]
            month = input_date[1]
            year = input_date[0]
            # for_date = datetime.datetime.strptime(request.GET.get('for'), '%Y-%m-%d')

        except:
            year = day = month = None
            # for_date = None

        print('for-', year, month, day)
        if year and month and day:
            user_bet = UserBet.objects.filter(
                user=request.user,
                did_win=True,
                created__year=year,
                created__month=month,
                created__day=day,

            )
        else:
            user_bet = UserBet.objects.filter(
                user=request.user,
                did_win=True,
            )

        serializer = UserBetWinSerializer(user_bet, many=True)
        return Response(serializer.data)


class TransferDiamond(APIView):
    """
    Transfer bet from player to penciler.
    """
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def post(self, request, format=None):
        diamond_to_transfer = request.data.get('diamond', None)
        try:
            user_diamond = UserProfile.objects.filter(user=request.user).first().diamond
        except:
            return Response({
                'success': False, 'msg': 'Unable to update diamonds. Please update User Profile.'})

        try:
            with transaction.atomic():

                print('user-diamond', user_diamond)
                if diamond_to_transfer > user_diamond:
                    # if user_diamond is 10 and diamond_to_transfer is 15 then throw error.
                    # updated user diamond can never be negative.
                    return Response({
                        'success': False, 'diamond': user_diamond, 'msg': 'Invalid diamond quantity to transfer!'})
                else:
                    updated_user_diamond = user_diamond - diamond_to_transfer

                # subtract from player
                UserProfile.objects.filter(
                    user=request.user, user_role=UserProfile.PLAYER
                ).update(diamond=updated_user_diamond)

                # add to penciler
                UserProfile.objects.filter(
                    user=request.user.users.supervisor,
                    user_role=UserProfile.PENCILER
                ).update(diamond=F('diamond') + diamond_to_transfer)

                return Response({'success': True, 'diamond': updated_user_diamond})  # send balance diamond

        except:
            print(
                traceback.format_exc()
            )
            # in case of failure return 'False' with user's current diamond.
            return Response({
                'success': False, 'diamond': user_diamond, 'msg': 'Unable to update diamonds. Please try again!'})
